﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica2_Pizzeria
{
	public partial class fondoMain : Form
	{
		private string[] pizzas = { "New York", "Vegetariana", "Barbacoa picante", "Cuatro quesos" };
		private string[] ingredientes = { "Jamon York", "Champi", "Calabacín", "Aceitunas", "Tabasco", "Ternera", "Piña", "Queso mozzarella", "Queso azul", "Queso parmesano", "Queso de cabra" };
		private string[] tipoMasa = { "Fina (+1€)", "Pan (+2€)", "Tradicional (+3€)", "Bordes Rellenos (+4€)" };
		private string[] tamañoPizza = { "Pequeño(+3€)", "Mediano (+4€)", "Familiar (+5€)" };
		private int contador, contadorG, precio;
		int[] acumular;
		CheckBox[] checkIngrediente;

		public fondoMain()
		{
			checkIngrediente = new CheckBox[11];
			// BackgroundImage = Practica2_Pizzeria.Properties.Resources.fondo;
			InitializeComponent();

			for (int i = 0; i < pizzas.Length; i++)
				listBTipo.Items.Add(pizzas[i]);
			comboBoxTamaño.DropDownStyle = ComboBoxStyle.DropDownList;
			comboBoxTipo.DropDownStyle = ComboBoxStyle.DropDownList;
            
		}


        private void listBTipo_SelectedValueChanged(object sender, MouseEventArgs e)
		{
			listCheckBox.Controls.Clear(); //Para mostrar los ingredientes sin que se concatenen
			btnAdd.Visible = true;
			labelExtra.Visible = true;
			listCheckBox.Visible = true;
			labelIngredientes.Visible = true;
			btnRemove.Visible = true;
			btnResumen.Visible = true;
			contadorG = 0;


			if (Equals(listBTipo.SelectedItem, pizzas[0]))  //Pizza New York
			{
				comboBoxTamaño.Items.Clear();
				comboBoxTipo.Items.Clear();
				listCheckBoxExtra.Controls.Clear();
               
				imagenPizza.BackgroundImage = Practica2_Pizzeria.Properties.Resources.nowyork;
				for (int i = 0; i < tamañoPizza.Length; i++)//Cargo todos los tipos de tamaños
				{
					comboBoxTamaño.Items.Add(tamañoPizza[i]);
				}
				comboBoxTamaño.SelectedIndex = 1; //Establezco predeterminado
				for (int i = 0; i < tipoMasa.Length; i++) //Cargo todos los tipos de pizzas
				{
					comboBoxTipo.Items.Add(tipoMasa[i]);
				}
				comboBoxTipo.SelectedIndex = 2;//Establezco predeterminado

				for (int i = 0; i < checkIngrediente.Length; i++) //Inicializo la el panel de checkBox individuales
				{
					checkIngrediente[i] = new CheckBox();
					listCheckBox.Controls.Add(checkIngrediente[i]); //Agregar el array de check al panel
					checkIngrediente[i].SetBounds(0, 0, 220, 27); //Para agrandarlo y no se superponga
					checkIngrediente[i].BackColor = Color.PowderBlue;
					checkIngrediente[i].Text = ingredientes[i];
				}
				//checkIngrediente[0]= checkBox1
				int[] nIngredientesBloqueados = { 0, 1 };
				for (int i = 0; i < nIngredientesBloqueados.Length; i++) //Le pongo el tick correcto y bloqueo 
				{
					listCheckBoxExtra.Controls.Add(checkIngrediente[i]);
					checkIngrediente[nIngredientesBloqueados[i]].Checked = true;
					checkIngrediente[nIngredientesBloqueados[i]].Enabled = false;
                
				}
                
			}
			if (Equals(listBTipo.SelectedItem, pizzas[1])) //Vegatariana
			{
				comboBoxTamaño.Items.Clear();
				comboBoxTipo.Items.Clear();
				listCheckBoxExtra.Controls.Clear();

				imagenPizza.BackgroundImage = Practica2_Pizzeria.Properties.Resources.vegetariana;


				comboBoxTamaño.Items.Add(tamañoPizza[0]);
				comboBoxTamaño.SelectedIndex = 0;
				for (int i = 0; i < tipoMasa.Length; i++)
				{
					comboBoxTipo.Items.Add(tipoMasa[i]);
				}
				comboBoxTipo.SelectedIndex = 2;
				int[] nIngredientes = { 2, 3, 4, 6, 7, 8, 9, 10 }; //Agrego todo los elementos de la pizza vegetariana
				for (int i = 0; i < nIngredientes.Length; i++)
				{
					checkIngrediente[i] = new CheckBox();
					listCheckBox.Controls.Add(checkIngrediente[i]);
					checkIngrediente[i].SetBounds(0, 0, 220, 27);
					checkIngrediente[i].BackColor = Color.PowderBlue;
					checkIngrediente[i].Text = ingredientes[nIngredientes[i]];
				}
				int[] nIngredientesBloqueados = { 0, 1 };
				for (int i = 0; i < nIngredientesBloqueados.Length; i++)
				{
					listCheckBoxExtra.Controls.Add(checkIngrediente[i]);
					checkIngrediente[nIngredientesBloqueados[i]].Checked = true;
					checkIngrediente[nIngredientesBloqueados[i]].Enabled = false;
				}

			}
			if (Equals(listBTipo.SelectedItem, pizzas[2]))
			{

				comboBoxTamaño.Items.Clear();
				comboBoxTipo.Items.Clear();
				listCheckBoxExtra.Controls.Clear();
				imagenPizza.BackgroundImage = Practica2_Pizzeria.Properties.Resources.barbacoa;

				for (int i = 0; i < tamañoPizza.Length; i++)
				{
					comboBoxTamaño.Items.Add(tamañoPizza[i]);
				}
				comboBoxTamaño.SelectedIndex = 1;
				for (int i = 0; i < tipoMasa.Length; i++)
				{
					comboBoxTipo.Items.Add(tipoMasa[i]);
				}
				comboBoxTipo.SelectedIndex = 2;

				for (int i = 0; i < checkIngrediente.Length; i++)
				{
					checkIngrediente[i] = new CheckBox();
					listCheckBox.Controls.Add(checkIngrediente[i]);
					checkIngrediente[i].SetBounds(0, 0, 220, 27);
					checkIngrediente[i].BackColor = Color.PowderBlue;
					checkIngrediente[i].Text = ingredientes[i];
				}
				//checkIngrediente[0]= checkBox1
				int[] nIngredientesBloqueados = { 4, 5 };
				for (int i = 0; i < nIngredientesBloqueados.Length; i++)
				{
					listCheckBoxExtra.Controls.Add(checkIngrediente[nIngredientesBloqueados[i]]); //ERROR
					checkIngrediente[nIngredientesBloqueados[i]].Checked = true;
					checkIngrediente[nIngredientesBloqueados[i]].Enabled = false;
				}
			}
			if (Equals(listBTipo.SelectedItem, pizzas[3]))  //Pizza 4 quesos
			{
				comboBoxTamaño.Items.Clear();
				comboBoxTipo.Items.Clear();
				listCheckBoxExtra.Controls.Clear();
				btnAdd.Visible = false;
				labelExtra.Visible = false;
				listCheckBox.Visible = false;
				btnRemove.Visible = false;
				imagenPizza.BackgroundImage = Practica2_Pizzeria.Properties.Resources.Quesos;
				int[] nIngredientes = { 7, 8, 9, 10 };
				for (int i = 0; i < nIngredientes.Length; i++) //Inicializo la el panel de checkBox individuales
				{
					checkIngrediente[i] = new CheckBox();
					listCheckBoxExtra.Controls.Add(checkIngrediente[i]); //Agregar el array de check al panel
					checkIngrediente[i].SetBounds(0, 0, 220, 27);
					checkIngrediente[i].BackColor = Color.PowderBlue;
					checkIngrediente[i].Text = ingredientes[nIngredientes[i]];
				}
				for (int i = 0; i < nIngredientes.Length; i++) //int[] nIngredientesBloqueados = todos
				{
					checkIngrediente[i].Checked = true;
					checkIngrediente[i].Enabled = false;
				}
				for (int i = 0; i < tamañoPizza.Length; i++)
				{
					comboBoxTamaño.Items.Add(tamañoPizza[i]);
				}
				comboBoxTamaño.SelectedIndex = 1;
				comboBoxTipo.Items.Add(tipoMasa[0]);
				comboBoxTipo.SelectedIndex = 0;
			}
		}

		private void btnRemove_Click(object sender, EventArgs e)
		{
			contador = 0;
			acumular = new int[4];
			for (int i = 0; i < checkIngrediente.Length; i++) //i cantidad ingredientes
			{
				for (int j = 0; j < listCheckBoxExtra.Controls.Count; j++) //ingredientes dentro de listderecha
				{
					if ((listCheckBoxExtra.Controls[j] == checkIngrediente[i])) //compara si esta en un lado 
					{
						if (checkIngrediente[i].Checked == false)
						{
							if (contador < 4)
								acumular[contador] = i; //el numero del ingrtediente que quiero remover
							contador++;
						}
					}
					if ((listCheckBoxExtra.Controls[j] == (listCheckBoxExtra.Controls[listCheckBoxExtra.Controls.Count - 1])) && (checkIngrediente[i] == (checkIngrediente[checkIngrediente.Length - 1])))
					{
						contadorG -= contador;
						if ((contadorG >= 0 && contadorG < 5) && ((contador >= 2) && (contador < 5)))
						{
							for (int h = 0; h < contador; h++)
							{
								listCheckBoxExtra.Controls.Remove(checkIngrediente[acumular[h]]);
								listCheckBox.Controls.Add(checkIngrediente[acumular[h]]);
								labelWarning.Visible = false;
							}
						}
						else
						{
							labelWarning.Text = "Solo  puede introducir o quitar\r\n  2,3,4 ingredientes a la vez\r\n  (Maximo 4 ingredientes)";
							labelWarning.Visible = true;
							contadorG += contador;
						}

					}
				}

			}

		}

		private void btnResumen_Click(object sender, EventArgs e)
		{
			btnCancelar.Visible = true;
			btnResumen.Visible = false;
			panelCreando.Enabled = false;
			int precio = 0;
			lblIngredientes.Text = "";
			lblCantidad.Text = "";
			lblTipoNombre.Text = "1 Pizza " + listBTipo.Text;
			lblTipoPizza.Text = "Tamaño:" + comboBoxTamaño.Text + "\r\nMasa:" + comboBoxTipo.Text;
			for (int i = 0; i < listCheckBoxExtra.Controls.Count; i++)
			{
				lblIngredientes.Text += listCheckBoxExtra.Controls[i].Text + "\r\n";
				/* if (listCheckBoxExtra.Controls[i].Enabled == false )
				 {*/
				if (i < 2 || listBTipo.Text == pizzas[3])
				{
					lblCantidad.Text += "+0.00€\r\n";

				}
				else
				{
					lblCantidad.Text += "+1.00€\r\n";
					precio++;
				}
			}
			for (int i = 0; i < comboBoxTipo.Items.Count; i++)
			{
				if (comboBoxTipo.Text == tipoMasa[i])
				{
					precio += i + 1;
				}
			}
			for (int i = 0; i < comboBoxTamaño.Items.Count; i++)
			{
				if (comboBoxTamaño.Text == tamañoPizza[i])
				{
					precio += i + 3;
				}
			}
			lblCantidadTotal.Text = precio + ".00€";
			PanelResumen.Visible = true;

		}

		private void button1_Click(object sender, EventArgs e)
		{
			MessageBox.Show("Importe calculado "+ lblCantidadTotal.Text, "Factura", MessageBoxButtons.OK, MessageBoxIcon.None);
		}




		private void fondoMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			DialogResult res = DialogResult.OK;
			if (res != MessageBox.Show("Estas seguro que deseas salir", "Salir", MessageBoxButtons.OKCancel, MessageBoxIcon.Question))
				e.Cancel = true;
		}

        private void lblTipoNombre_Click(object sender, EventArgs e)
        {

        }

        private void lblTipoPizza_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
		{
			btnCancelar.Visible = false;
			btnResumen.Visible = true;
			PanelResumen.Visible = false;
			panelCreando.Enabled = true;
		}

		private void checkBox_CheckedChanged(object sender, EventArgs e)
		{
			contador = 0;
			acumular = new int[4];

			for (int i = 0; i < checkIngrediente.Length; i++) //i cantidad ingredientes
			{
				for (int j = 0; j < listCheckBox.Controls.Count; j++) //ingredientes dentro de listderecha
				{
					if ((listCheckBox.Controls[j] == checkIngrediente[i]) && contador < 5) //compara si esta en un lado 
					{
						if (checkIngrediente[i].Checked == true)
						{
							if (contador < 4)
								acumular[contador] = i; //el numero del ingrtediente que quiero remover
							contador++;
						}
					}
					if ((listCheckBox.Controls[j] == (listCheckBox.Controls[listCheckBox.Controls.Count - 1])) && (checkIngrediente[i] == (checkIngrediente[checkIngrediente.Length - 1])))
					{
						contadorG += contador;
						if ((contadorG < 5 && contadorG >= 2) && ((contador >= 2) && (contador < 5)))
						{

							for (int h = 0; h < contador; h++)
							{
								listCheckBox.Controls.Remove(checkIngrediente[acumular[h]]);
								listCheckBoxExtra.Controls.Add(checkIngrediente[acumular[h]]);
								labelWarning.Visible = false;
							}
						}
						else
						{
							labelWarning.Text = "Solo  puede introducir o quitar\r\n  2,3,4 ingredientes a la vez\r\n  (Maximo 4 ingredientes)";
							labelWarning.Visible = true;
							contadorG -= contador;
						}
					}
				}
			}
		}
	}
}
