﻿namespace Practica2_Pizzeria
{
    partial class fondoMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fondoMain));
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.btnResumen = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.PanelResumen = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.lblCantidadTotal = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.lblIngredientes = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTipoPizza = new System.Windows.Forms.Label();
            this.lblTipoNombre = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.listBTipo = new System.Windows.Forms.ListBox();
            this.listCheckBox = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxTipo = new System.Windows.Forms.ComboBox();
            this.comboBoxTamaño = new System.Windows.Forms.ComboBox();
            this.imagenPizza = new System.Windows.Forms.PictureBox();
            this.listCheckBoxExtra = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.labelIngredientes = new System.Windows.Forms.Label();
            this.labelExtra = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.labelWarning = new System.Windows.Forms.Label();
            this.panelCreando = new System.Windows.Forms.Panel();
            this.PanelResumen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagenPizza)).BeginInit();
            this.panelCreando.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(218, -112);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(131, 27);
            this.checkBox1.TabIndex = 10;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // btnResumen
            // 
            this.btnResumen.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btnResumen.Location = new System.Drawing.Point(522, 561);
            this.btnResumen.Name = "btnResumen";
            this.btnResumen.Size = new System.Drawing.Size(195, 47);
            this.btnResumen.TabIndex = 13;
            this.btnResumen.Text = "Resumen";
            this.btnResumen.UseVisualStyleBackColor = true;
            this.btnResumen.Visible = false;
            this.btnResumen.Click += new System.EventHandler(this.btnResumen_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(522, 561);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(195, 47);
            this.btnCancelar.TabIndex = 14;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // PanelResumen
            // 
            this.PanelResumen.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.PanelResumen.Controls.Add(this.button1);
            this.PanelResumen.Controls.Add(this.lblCantidadTotal);
            this.PanelResumen.Controls.Add(this.label10);
            this.PanelResumen.Controls.Add(this.label9);
            this.PanelResumen.Controls.Add(this.lblCantidad);
            this.PanelResumen.Controls.Add(this.lblIngredientes);
            this.PanelResumen.Controls.Add(this.label6);
            this.PanelResumen.Controls.Add(this.lblTipoPizza);
            this.PanelResumen.Controls.Add(this.lblTipoNombre);
            this.PanelResumen.Controls.Add(this.label4);
            this.PanelResumen.Location = new System.Drawing.Point(768, 111);
            this.PanelResumen.Name = "PanelResumen";
            this.PanelResumen.Size = new System.Drawing.Size(373, 523);
            this.PanelResumen.TabIndex = 15;
            this.PanelResumen.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(184, 470);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(165, 38);
            this.button1.TabIndex = 6;
            this.button1.Text = "Pagar importe";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblCantidadTotal
            // 
            this.lblCantidadTotal.AutoSize = true;
            this.lblCantidadTotal.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidadTotal.Location = new System.Drawing.Point(184, 434);
            this.lblCantidadTotal.Name = "lblCantidadTotal";
            this.lblCantidadTotal.Size = new System.Drawing.Size(107, 29);
            this.lblCantidadTotal.TabIndex = 5;
            this.lblCantidadTotal.Text = "15.00€";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(57, 434);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 29);
            this.label10.TabIndex = 5;
            this.label10.Text = "Total:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 409);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(325, 23);
            this.label9.TabIndex = 4;
            this.label9.Text = "-----------------------------------";
            // 
            // lblCantidad
            // 
            this.lblCantidad.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCantidad.AutoSize = true;
            this.lblCantidad.Location = new System.Drawing.Point(278, 156);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(57, 253);
            this.lblCantidad.TabIndex = 3;
            this.lblCantidad.Text = "0\r\n0\r\n0\r\n0\r\n+1.0\r\n+1.0\r\n+1.0\r\n+1.0\r\n+1.0\r\n+1.0\r\n+1.0";
            this.lblCantidad.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblIngredientes
            // 
            this.lblIngredientes.AutoSize = true;
            this.lblIngredientes.Location = new System.Drawing.Point(28, 156);
            this.lblIngredientes.Name = "lblIngredientes";
            this.lblIngredientes.Size = new System.Drawing.Size(119, 253);
            this.lblIngredientes.TabIndex = 3;
            this.lblIngredientes.Text = "Piña\r\nMerme\r\nsdaddas\r\nasdsdasdas\r\nsdsdasd\r\nsdsdads\r\nasdadas\r\nsdadad\r\nasddas\r\nasdd" +
    "ds\r\nsdadsads";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(164, 23);
            this.label6.TabIndex = 2;
            this.label6.Text = "Ingredientes: ";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // lblTipoPizza
            // 
            this.lblTipoPizza.AutoSize = true;
            this.lblTipoPizza.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoPizza.Location = new System.Drawing.Point(25, 83);
            this.lblTipoPizza.Name = "lblTipoPizza";
            this.lblTipoPizza.Size = new System.Drawing.Size(71, 18);
            this.lblTipoPizza.TabIndex = 1;
            this.lblTipoPizza.Text = "1 Pizza";
            this.lblTipoPizza.Click += new System.EventHandler(this.lblTipoPizza_Click);
            // 
            // lblTipoNombre
            // 
            this.lblTipoNombre.AutoSize = true;
            this.lblTipoNombre.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoNombre.Location = new System.Drawing.Point(23, 47);
            this.lblTipoNombre.Name = "lblTipoNombre";
            this.lblTipoNombre.Size = new System.Drawing.Size(96, 25);
            this.lblTipoNombre.TabIndex = 1;
            this.lblTipoNombre.Text = "1 Pizza";
            this.lblTipoNombre.Click += new System.EventHandler(this.lblTipoNombre_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(324, 32);
            this.label4.TabIndex = 0;
            this.label4.Text = "Detalles del pedido: ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::Practica2_Pizzeria.Properties.Resources.fondo;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1167, 647);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // listBTipo
            // 
            this.listBTipo.FormattingEnabled = true;
            this.listBTipo.ItemHeight = 23;
            this.listBTipo.Location = new System.Drawing.Point(10, 32);
            this.listBTipo.Name = "listBTipo";
            this.listBTipo.Size = new System.Drawing.Size(219, 96);
            this.listBTipo.TabIndex = 1;
            this.listBTipo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBTipo_SelectedValueChanged);
            // 
            // listCheckBox
            // 
            this.listCheckBox.Location = new System.Drawing.Point(499, 32);
            this.listCheckBox.Name = "listCheckBox";
            this.listCheckBox.Size = new System.Drawing.Size(198, 311);
            this.listCheckBox.TabIndex = 4;
            this.listCheckBox.Visible = false;
            // 
            // comboBoxTipo
            // 
            this.comboBoxTipo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTipo.FormattingEnabled = true;
            this.comboBoxTipo.Location = new System.Drawing.Point(10, 154);
            this.comboBoxTipo.Name = "comboBoxTipo";
            this.comboBoxTipo.Size = new System.Drawing.Size(219, 26);
            this.comboBoxTipo.TabIndex = 5;
            // 
            // comboBoxTamaño
            // 
            this.comboBoxTamaño.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTamaño.FormattingEnabled = true;
            this.comboBoxTamaño.Location = new System.Drawing.Point(10, 213);
            this.comboBoxTamaño.Name = "comboBoxTamaño";
            this.comboBoxTamaño.Size = new System.Drawing.Size(219, 26);
            this.comboBoxTamaño.TabIndex = 5;
            // 
            // imagenPizza
            // 
            this.imagenPizza.Location = new System.Drawing.Point(31, 251);
            this.imagenPizza.Name = "imagenPizza";
            this.imagenPizza.Size = new System.Drawing.Size(164, 144);
            this.imagenPizza.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imagenPizza.TabIndex = 6;
            this.imagenPizza.TabStop = false;
            // 
            // listCheckBoxExtra
            // 
            this.listCheckBoxExtra.Location = new System.Drawing.Point(235, 32);
            this.listCheckBoxExtra.Name = "listCheckBoxExtra";
            this.listCheckBoxExtra.Size = new System.Drawing.Size(198, 363);
            this.listCheckBoxExtra.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Seleccione pizza.";
            // 
            // labelIngredientes
            // 
            this.labelIngredientes.AutoSize = true;
            this.labelIngredientes.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIngredientes.Location = new System.Drawing.Point(232, 11);
            this.labelIngredientes.Name = "labelIngredientes";
            this.labelIngredientes.Size = new System.Drawing.Size(120, 18);
            this.labelIngredientes.TabIndex = 9;
            this.labelIngredientes.Text = "Ingredientes:";
            this.labelIngredientes.Visible = false;
            // 
            // labelExtra
            // 
            this.labelExtra.AutoSize = true;
            this.labelExtra.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExtra.Location = new System.Drawing.Point(493, 11);
            this.labelExtra.Name = "labelExtra";
            this.labelExtra.Size = new System.Drawing.Size(211, 18);
            this.labelExtra.TabIndex = 9;
            this.labelExtra.Text = "Ingrediente extra(+1€): ";
            this.labelExtra.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 18);
            this.label2.TabIndex = 9;
            this.label2.Text = "Tipo de masa.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 18);
            this.label3.TabIndex = 9;
            this.label3.Text = "Tamaño deseado.";
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnAdd.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(438, 164);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(58, 43);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Añadir";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Visible = false;
            this.btnAdd.Click += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnRemove.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Location = new System.Drawing.Point(438, 213);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(58, 43);
            this.btnRemove.TabIndex = 12;
            this.btnRemove.Text = "Quitar";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Visible = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // labelWarning
            // 
            this.labelWarning.AutoSize = true;
            this.labelWarning.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.labelWarning.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWarning.ForeColor = System.Drawing.Color.Red;
            this.labelWarning.Location = new System.Drawing.Point(463, 368);
            this.labelWarning.Name = "labelWarning";
            this.labelWarning.Size = new System.Drawing.Size(0, 16);
            this.labelWarning.TabIndex = 11;
            // 
            // panelCreando
            // 
            this.panelCreando.BackColor = System.Drawing.Color.Transparent;
            this.panelCreando.Controls.Add(this.btnRemove);
            this.panelCreando.Controls.Add(this.labelWarning);
            this.panelCreando.Controls.Add(this.btnAdd);
            this.panelCreando.Controls.Add(this.label3);
            this.panelCreando.Controls.Add(this.label2);
            this.panelCreando.Controls.Add(this.labelExtra);
            this.panelCreando.Controls.Add(this.labelIngredientes);
            this.panelCreando.Controls.Add(this.label1);
            this.panelCreando.Controls.Add(this.listCheckBoxExtra);
            this.panelCreando.Controls.Add(this.imagenPizza);
            this.panelCreando.Controls.Add(this.comboBoxTamaño);
            this.panelCreando.Controls.Add(this.comboBoxTipo);
            this.panelCreando.Controls.Add(this.listCheckBox);
            this.panelCreando.Controls.Add(this.listBTipo);
            this.panelCreando.Location = new System.Drawing.Point(12, 111);
            this.panelCreando.Name = "panelCreando";
            this.panelCreando.Size = new System.Drawing.Size(705, 418);
            this.panelCreando.TabIndex = 16;
            // 
            // fondoMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1167, 645);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnResumen);
            this.Controls.Add(this.PanelResumen);
            this.Controls.Add(this.panelCreando);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "fondoMain";
            this.Text = "Pizzeria la natural";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.fondoMain_FormClosing);
            this.PanelResumen.ResumeLayout(false);
            this.PanelResumen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagenPizza)).EndInit();
            this.panelCreando.ResumeLayout(false);
            this.panelCreando.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button btnResumen;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Panel PanelResumen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblCantidadTotal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.Label lblIngredientes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTipoNombre;
        private System.Windows.Forms.Label lblTipoPizza;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListBox listBTipo;
        private System.Windows.Forms.FlowLayoutPanel listCheckBox;
        private System.Windows.Forms.ComboBox comboBoxTipo;
        private System.Windows.Forms.ComboBox comboBoxTamaño;
        private System.Windows.Forms.PictureBox imagenPizza;
        private System.Windows.Forms.FlowLayoutPanel listCheckBoxExtra;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelIngredientes;
        private System.Windows.Forms.Label labelExtra;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Label labelWarning;
        private System.Windows.Forms.Panel panelCreando;
    }
}

