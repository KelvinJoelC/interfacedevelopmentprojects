
import java.io.*;

import javax.swing.ImageIcon;

public class CargarFichero {
	private File usuarios = new File("src/Textos/Usuarios.txt");
	private File texto1 = new File("src/Textos/Textos.txt");
	private File estadisticas = new File("src/Textos/Estadisticas.txt");

	//VentanaInicio ventanaInicio = new VentanaInicio();

	public boolean comprobarFicheros() {

		if (usuarios.exists() != true || texto1.exists() != true || estadisticas.exists() != true) {
			return false;
		} else
			return true;
	}

	public String dameTexto(int tipo)
	{
		String Texto1;
		String Texto2;
		String txt;
		Texto1 = "";
		Texto2 = "";
		txt = "";
		
	
		try {			
			FileReader frUsuarios = new FileReader(texto1);
			BufferedReader bf = new BufferedReader(frUsuarios);

			while ((txt = bf.readLine()) != null) {

				Texto1 = txt.substring(txt.indexOf('/') + 1, txt.indexOf('-'));
				Texto2 = txt.substring(txt.indexOf("-") + 1, txt.indexOf('#'));
			}
			
			bf.close();

		} catch (IOException e) {
			System.out.println("Error al cargar el fichero usuario");
		}
		if(tipo==0) {

			return Texto1;
		}else
		return Texto2;
		
	}
	
	public boolean cargaFicheroUsuarios(String User, String Pass) {
		

		String Usuario;
		String Contraseña;
		String txt;
		Usuario = "";
		Contraseña = "";
		txt = "";
		try {			
			FileReader frUsuarios = new FileReader(usuarios);
			BufferedReader bf = new BufferedReader(frUsuarios);

			while ((txt = bf.readLine()) != null) {

				Usuario = txt.substring(txt.indexOf('/') + 1, txt.indexOf('-'));
				Contraseña = txt.substring(txt.indexOf('-') + 1, txt.indexOf(';'));
				
				if (Usuario.equals(User)) {
					if (Contraseña.equals(Pass)) {
						//Algo
						bf.close();
						return true;
					}
				}
			}
			
			bf.close();

		} catch (IOException e) {
			System.out.println("Error al cargar el fichero usuario");
		}
		return false;
	}
	
	public void guardaEstadisticas(String User,String leccionActual, int pulTotales, int tiempo,int errores )
	{
		String textoAuxUltimo="";
		String textoAuxGlobal="";
		String NRegistro="";
		String Usuario="";
		String Tiempo="";
		String PulsacionesTotales="";
		String Fallos="";
		String Leccion="";
		String txt="";
		

		try {			
			FileReader frEstadisticas = new FileReader(estadisticas);
			BufferedReader bf = new BufferedReader(frEstadisticas);

			while ((txt = bf.readLine()) != null) {
				
				textoAuxGlobal += txt+"\n";
				NRegistro = txt.substring(txt.indexOf('/') + 1, txt.indexOf('-'));
				Usuario = txt.substring(txt.indexOf('-') + 1, txt.indexOf('*'));
				Leccion= txt.substring(txt.indexOf('*') + 1, txt.indexOf('¬'));
				Tiempo = txt.substring(txt.indexOf('¬') + 1, txt.indexOf('$'));
				PulsacionesTotales = txt.substring(txt.indexOf('$') + 1, txt.indexOf('#'));
				Fallos = txt.substring(txt.indexOf('#') + 1, txt.indexOf('€'));
				
				textoAuxUltimo="/"+NRegistro+"-"+Usuario+"*"+Leccion+"¬"+Tiempo+"$"+PulsacionesTotales+"#"+Fallos+"€";
				//System.out.println(textoAuxGlobal);*/
				Integer.parseInt(NRegistro);
			}
			
			bf.close();

		} catch (IOException e) {
			System.out.println("Error al cargar el fichero usuario");
		}
		//Escribimos lo leido	
		try
		{
		FileWriter fw= new FileWriter(estadisticas);
		
		// Escribir en el fichero
		fw.write(textoAuxGlobal);
		fw.write("/"+(Integer.parseInt(NRegistro)+1)+"-"+User+"*"+leccionActual+"¬"+tiempo+"$"+pulTotales+"#"+errores+"€\n");
		//Cerrar Fichero
		fw.close();
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
		}
		
	}

	public String muestraEstadisticas(String User)
	{
		String textoAuxUltimo="";
		String NRNuevo="";
		String NRegistro="";
		String Usuario="";
		String Tiempo="";
		String PulsacionesTotales="";
		String Fallos="";
		String Leccion="";
		String txt="";
		

		try {			
			FileReader frEstadisticas = new FileReader(estadisticas);
			BufferedReader bf = new BufferedReader(frEstadisticas);

			while ((txt = bf.readLine()) != null) {
				
				
				NRegistro = txt.substring(txt.indexOf('/') + 1, txt.indexOf('-'));
				Usuario = txt.substring(txt.indexOf('-') + 1, txt.indexOf('*'));
				Leccion= txt.substring(txt.indexOf('*') + 1, txt.indexOf('¬'));
				Tiempo = txt.substring(txt.indexOf('¬') + 1, txt.indexOf('$'));
				PulsacionesTotales = txt.substring(txt.indexOf('$') + 1, txt.indexOf('#'));
				Fallos = txt.substring(txt.indexOf('#') + 1, txt.indexOf('€'));
				
				
				if(User.equals(Usuario)){
					if(!NRegistro.equals(NRNuevo)) {
						textoAuxUltimo+=
						"Numero de registro: "+NRegistro+".\n"
						+"Leccion:"+Leccion+".\n"
						+"Tiempo: "+Tiempo+" segundos.\n"
						+"Pulsaciones Totales: "+PulsacionesTotales+"\n"
						+"Numero de fallos: "+Fallos+"\n\n";
					}
				NRNuevo=NRegistro;
				}
			}

			bf.close();

		} catch (IOException e) {
			System.out.println("Error al cargar el fichero usuario");
		}
		//System.out.println(textoAuxUltimo);
		return textoAuxUltimo;
	}


}
