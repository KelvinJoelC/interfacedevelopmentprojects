import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JEditorPane;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.Highlighter.HighlightPainter;

import java.awt.Window.Type;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.DecimalFormat;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.SystemColor;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class VentanaPrincipal {

	private JFrame frame;
	private Timer time;
	private int tipoTexto;
	private int posicionCursor;

	CargarFichero leeFichero = new CargarFichero();
	private String usuarioActual = "";
	private String leccionA = "";

	public JFrame getFrame() {
		return frame;
	}

	public char letra[];

	public int hora, minuto, segundo, milesimas, segundoGlobal, errores;
	public double contadorPulsacionesXMin;

	public VentanaPrincipal(int tipoText, String usuario, String Leccion) {
		hora = 0;
		minuto = 0;
		segundo = 0;
		milesimas = 0;
		contadorPulsacionesXMin = 0;
		segundoGlobal = 0;
		errores = 0;
		tipoTexto = tipoText;
		usuarioActual = usuario;
		leccionA = Leccion;
		letra = leeFichero.dameTexto(tipoTexto).toCharArray();

		initialize();
		time.start();
	}

	public void setUsuarioActual(String usuarioActual) {
		this.usuarioActual = usuarioActual;
	}

	private void initialize() {

		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (JOptionPane.showConfirmDialog(frame,
						"Vas a salir del programa sin guardar\n" + "�Estas seguro?", "Cerrar el programa",
						JOptionPane.YES_NO_OPTION) == 0) {
					VentanaInicio ventanaInicio = new VentanaInicio(usuarioActual);
					ventanaInicio.getFrame().setVisible(true);
					frame.dispose();
				}else frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			}
		});
		
		frame.setType(Type.POPUP);
		frame.setAlwaysOnTop(false);
		frame.setBounds(0, 0, 1982, 1056);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setLocationRelativeTo(null);// Para que te salga centrado
		frame.getContentPane().setLayout(null);
		frame.setResizable(false); // cambiar
		frame.setAlwaysOnTop(true);
		
		frame.setTitle("Mecanograf\u00EDa.");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaInicio.class.getResource("/Images/DosaIconMin.png")));
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		JMenu mnInformacin = new JMenu("Acerca de");
		menuBar.add(mnInformacin);
		JLabel lblVersin = new JLabel("Versi\u00F3n: 1.0");
		mnInformacin.add(lblVersin);
		JLabel lblRealizadoPorKelvin = new JLabel("Realizado por: Kelvin Carre\u00F1o");
		mnInformacin.add(lblRealizadoPorKelvin);
		JLabel lblUltimaActualizaccin = new JLabel("Ultima actualizaci\u00F3n: 14/11/2019 ");
		mnInformacin.add(lblUltimaActualizaccin);

		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		int height = pantalla.height;
		int width = pantalla.width;

		// CONTENEDOR TEXTO

		JPanel ContenedorTexto = new JPanel();
		ContenedorTexto.setBackground(SystemColor.control);
		ContenedorTexto.setBounds(0, 0, (7 * width / 8), height / 2);
		frame.getContentPane().add(ContenedorTexto);
		ContenedorTexto.setLayout(null);

		JTextPane txtTexto = new JTextPane();
		txtTexto.setAutoscrolls(false);
		txtTexto.setFocusCycleRoot(false);
		txtTexto.setBackground(SystemColor.controlHighlight);
		int margenTexto = 30;
		txtTexto.setEditable(false);
		txtTexto.setFont(new Font("Tahoma", Font.PLAIN, 31));
		txtTexto.setText(leeFichero.dameTexto(tipoTexto));
		txtTexto.setBounds(margenTexto, margenTexto, (7 * width / 8) - margenTexto * 2, height / 2 - margenTexto * 2);
		ContenedorTexto.add(txtTexto);

		// CONTENEDOR BARRA LATERAL

		JPanel ContenedorBarraLateral = new JPanel();

		ContenedorBarraLateral.setBounds(7 * width / 8, 0, width / 8, height);
		frame.getContentPane().add(ContenedorBarraLateral);
		ContenedorBarraLateral.setLayout(new GridLayout(5, 1, 0, 0));

		JPanel ContenedorContadorPulsacionesTotales = new JPanel();
		ContenedorContadorPulsacionesTotales.setBackground(SystemColor.controlHighlight);
		Dimension DimContenedor = ContenedorBarraLateral.getSize();
		int anchoContenedor = DimContenedor.width / 4;
		ContenedorBarraLateral.add(ContenedorContadorPulsacionesTotales);
		ContenedorContadorPulsacionesTotales.setLayout(null);

		JLabel lblTituloContadorTotal = new JLabel("Pulsaciones totales");
		lblTituloContadorTotal.setBounds(2 * anchoContenedor / 3, 50, 3 * anchoContenedor, 44);
		lblTituloContadorTotal.setFont(new Font("Tahoma", Font.BOLD, 18));
		ContenedorContadorPulsacionesTotales.add(lblTituloContadorTotal);

		JLabel lblVariableContadorTotal = new JLabel("0");
		lblVariableContadorTotal.setBounds(anchoContenedor * 2, 111, 3 * anchoContenedor, 44);
		lblVariableContadorTotal.setFont(new Font("Tahoma", Font.BOLD, 24));
		ContenedorContadorPulsacionesTotales.add(lblVariableContadorTotal);

		JPanel ContenedorContadorPulsaciones = new JPanel();
		ContenedorContadorPulsaciones.setBackground(SystemColor.controlHighlight);
		ContenedorBarraLateral.add(ContenedorContadorPulsaciones);
		ContenedorContadorPulsaciones.setLayout(null);

		JLabel lblTituloContador = new JLabel("Pulsaciones X Min");
		lblTituloContador.setBounds(2 * anchoContenedor / 3, 50, 3 * anchoContenedor, 44);
		lblTituloContador.setFont(new Font("Tahoma", Font.BOLD, 18));
		ContenedorContadorPulsaciones.add(lblTituloContador);

		JLabel lblVariableContador = new JLabel("xxxxxxx");
		lblVariableContador.setBounds(80, 111, 3 * anchoContenedor, 44);
		lblVariableContador.setFont(new Font("Tahoma", Font.BOLD, 16));
		ContenedorContadorPulsaciones.add(lblVariableContador);

		JPanel ContenedorContadorFallos = new JPanel();
		ContenedorContadorFallos.setBackground(SystemColor.controlHighlight);
		ContenedorBarraLateral.add(ContenedorContadorFallos);
		ContenedorContadorFallos.setLayout(null);

		JLabel lblTitulosContadorFallos = new JLabel("Fallos");
		lblTitulosContadorFallos.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblTitulosContadorFallos.setBounds(4 * anchoContenedor / 3, 50, 3 * anchoContenedor, 44);
		ContenedorContadorFallos.add(lblTitulosContadorFallos);

		JLabel lblVariableContadorFallos = new JLabel(errores + "");
		lblVariableContadorFallos.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblVariableContadorFallos.setBounds(5 * anchoContenedor / 3, 50 * 2, 3 * anchoContenedor, 44);
		ContenedorContadorFallos.add(lblVariableContadorFallos);

		JPanel ContenedorTiempo = new JPanel();
		ContenedorTiempo.setBackground(SystemColor.controlHighlight);
		ContenedorBarraLateral.add(ContenedorTiempo);
		ContenedorTiempo.setLayout(null);

		JLabel lblTiempoTranscurrido = new JLabel("Tiempo transcurrido");
		lblTiempoTranscurrido.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblTiempoTranscurrido.setBounds(34, 56, 183, 44);
		ContenedorTiempo.add(lblTiempoTranscurrido);

		JLabel lblTituloTiempo = new JLabel("0:0:0");
		lblTituloTiempo.setFont(new Font("Tahoma", Font.BOLD, 28));
		lblTituloTiempo.setBounds(84, 111, 3 * anchoContenedor, 44);
		ContenedorTiempo.add(lblTituloTiempo);

		JPanel ContenedorBotones = new JPanel();

		ContenedorBotones.setBounds(0, height / 2, (7 * width / 8), height / 2);
		frame.getContentPane().add(ContenedorBotones);
		ContenedorBotones.setLayout(null);

		JPanel btnNumeros = new JPanel();
		btnNumeros.setBounds(0, 0, 1450, 95);
		ContenedorBotones.add(btnNumeros);
		btnNumeros.setLayout(new GridLayout(1, 10, 0, 0));

		JButton btn1 = new JButton("1");
		btn1.setName("ghf");
		btn1.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnNumeros.add(btn1);

		JButton btn2 = new JButton("2");
		btn2.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnNumeros.add(btn2);

		JButton btn3 = new JButton("3");
		btn3.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnNumeros.add(btn3);

		JButton btn4 = new JButton("4");
		btn4.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnNumeros.add(btn4);

		JButton btn5 = new JButton("5");
		btn5.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnNumeros.add(btn5);

		JButton btn6 = new JButton("6");
		btn6.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnNumeros.add(btn6);

		JButton btn7 = new JButton("7");
		btn7.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnNumeros.add(btn7);

		JButton btn8 = new JButton("8");
		btn8.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnNumeros.add(btn8);

		JButton btn9 = new JButton("9");
		btn9.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnNumeros.add(btn9);

		JButton btn0 = new JButton("0");
		btn0.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnNumeros.add(btn0);

		JPanel btnFila1 = new JPanel();
		btnFila1.setBounds(80, 95, 1450, 95);
		ContenedorBotones.add(btnFila1);
		btnFila1.setLayout(new GridLayout(1, 10, 0, 0));

		JButton btnQ = new JButton("Q");
		btnQ.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila1.add(btnQ);

		JButton btnW = new JButton("W");
		btnW.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila1.add(btnW);

		JButton btnE = new JButton("E");
		btnE.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila1.add(btnE);

		JButton btnR = new JButton("R");
		btnR.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila1.add(btnR);

		JButton btnT = new JButton("T");
		btnT.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila1.add(btnT);

		JButton btnY = new JButton("Y");
		btnY.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila1.add(btnY);

		JButton btnU = new JButton("U");
		btnU.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila1.add(btnU);

		JButton btnI = new JButton("I");
		btnI.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila1.add(btnI);

		JButton btnO = new JButton("O");
		btnO.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila1.add(btnO);

		JButton btnP = new JButton("P");
		btnP.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila1.add(btnP);

		JPanel btnFila2 = new JPanel();
		btnFila2.setBounds(160, 190, 1450, 95);
		ContenedorBotones.add(btnFila2);
		btnFila2.setLayout(new GridLayout(0, 10, 0, 0));

		JButton btnA = new JButton("A");
		btnA.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila2.add(btnA);

		JButton btnS = new JButton("S");
		btnS.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila2.add(btnS);

		JButton btnD = new JButton("D");
		btnD.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila2.add(btnD);

		JButton btnF = new JButton("F");
		btnF.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila2.add(btnF);

		JButton btnG = new JButton("G");
		btnG.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila2.add(btnG);

		JButton btnH = new JButton("H");
		btnH.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila2.add(btnH);

		JButton btnJ = new JButton("J");
		btnJ.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila2.add(btnJ);

		JButton btnK = new JButton("K");
		btnK.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila2.add(btnK);

		JButton btnL = new JButton("L");
		btnL.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila2.add(btnL);

		JButton btn� = new JButton("\u00D1");
		btn�.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila2.add(btn�);

		JPanel btnFila3 = new JPanel();
		btnFila3.setBounds(230, 285, 1000, 95);
		ContenedorBotones.add(btnFila3);
		btnFila3.setLayout(new GridLayout(0, 7, 0, 0));

		JButton btnZ = new JButton("Z");
		btnZ.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila3.add(btnZ);

		JButton btnX = new JButton("X");
		btnX.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila3.add(btnX);

		JButton btnC = new JButton("C");
		btnC.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila3.add(btnC);

		JButton btnV = new JButton("V");
		btnV.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila3.add(btnV);

		JButton btnB = new JButton("B");
		btnB.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila3.add(btnB);

		JButton btnN = new JButton("N");
		btnN.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila3.add(btnN);

		JButton btnM = new JButton("M");
		btnM.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnFila3.add(btnM);

		JPanel btnEspaciador = new JPanel();
		btnEspaciador.setBounds(320, 380, 800, 95);
		ContenedorBotones.add(btnEspaciador);
		btnEspaciador.setLayout(new GridLayout(0, 1, 0, 0));

		JButton btnEspacio = new JButton("Espacio");
		btnEspacio.setFont(new Font("Sylfaen", Font.BOLD, 35));
		btnEspaciador.add(btnEspacio);
		// FUNCION TIMER

		time = new Timer(100, new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				frame.requestFocus();
				if (contadorPulsacionesXMin >= 1) // Para que empieze a contar cuando pulse por primera vez
				{
					
					milesimas++;
					// segundo++;

					DecimalFormat formato = new DecimalFormat("#.00");// Para mostrar solo dos digitos en double
					if (contadorPulsacionesXMin * 60 / segundoGlobal >= 100000000) {
						lblVariableContador.setText("Muy rapido");
					} else
						lblVariableContador.setText(formato.format(contadorPulsacionesXMin * 60 / segundoGlobal) + "PxM"); 
					if (milesimas >= 10) {
						milesimas = 0;
						segundoGlobal++;
						segundo++;
					}
					if (segundo >= 60) {
						segundo = 0;
						minuto++;
					}
					if (minuto >= 60) {
						minuto = 0;
						hora++;
					}
					if (hora >= 24) {
						segundo = 0;
						minuto = 0;
						hora = 0;
					}
					lblTituloTiempo.setText(hora + ":" + minuto + ":" + segundo);
				}
			}
		});

		JPanel ContenedorSalir = new JPanel();
		ContenedorSalir.setBackground(SystemColor.controlHighlight);
		ContenedorSalir.setVisible(true); // Modificar a cuando haya acabado
		ContenedorBarraLateral.add(ContenedorSalir);
		ContenedorSalir.setLayout(null);

		JLabel lblIconSalir = new JLabel("ImagenSalir");
		lblIconSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (JOptionPane.showConfirmDialog(frame,
						"Vas a salir del programa guardando autom�ticamente\n" + "�Estas seguro?", "Retroceder",
						JOptionPane.YES_NO_OPTION) == 0) {
					leeFichero.guardaEstadisticas(usuarioActual, leccionA, (int) contadorPulsacionesXMin, segundoGlobal,
							errores);
					VentanaInicio ventanaInicio = new VentanaInicio(usuarioActual);
					
					
					//ventanaInicio.setTFUserLogin(usuarioActual);
					//ventanaInicio.cargadoLogin(ventanaInicio.getTFUserLogin());
					ventanaInicio.getFrame().setVisible(true);
					
					frame.dispose();
				}
			}
		});
		lblIconSalir.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/Images/IconoSalir.png")));
		lblIconSalir.setBounds(5 * anchoContenedor / 3, 30, 67, 63);
		ContenedorSalir.add(lblIconSalir);

		JLabel lblSalirInformacion = new JLabel("Salir y guardar");
		lblSalirInformacion.setForeground(Color.RED);
		lblSalirInformacion.setBounds(5 * anchoContenedor / 3 - 8, 93, 85, 14);
		ContenedorSalir.add(lblSalirInformacion);
		
		frame.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent e) {

			}

			public void keyPressed(KeyEvent e) {

				if (e.getKeyCode() == KeyEvent.VK_B) {
					btnB.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);

				}
				if (e.getKeyCode() == KeyEvent.VK_A) {
					btnA.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_C) {
					btnC.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_D) {
					btnD.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_E) {
					btnE.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_F) {
					btnF.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_G) {
					btnG.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_H) {
					btnH.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_I) {
					btnI.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_J) {
					btnJ.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_K) {
					btnK.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_L) {
					btnL.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_M) {
					btnM.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == 0) {
					btn�.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}

				if (e.getKeyCode() == KeyEvent.VK_N) {
					btnN.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_O) {
					btnO.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_P) {
					btnP.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_Q) {
					btnQ.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_R) {
					btnR.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_S) {
					btnS.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_T) {
					btnT.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_U) {
					btnU.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_V) {
					btnV.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_W) {
					btnW.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_X) {
					btnX.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_Y) {
					btnY.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_Z) {
					btnZ.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					btnEspacio.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_1) {
					btn1.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_2) {
					btn2.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_3) {
					btn3.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_4) {
					btn4.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_5) {
					btn5.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_6) {
					btn6.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_7) {
					btn7.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_8) {
					btn8.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_9) {
					btn9.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
				if (e.getKeyCode() == KeyEvent.VK_0) {
					btn0.setBackground(Color.PINK);
					pintaDondeQuieras(txtTexto, lblVariableContadorFallos,lblVariableContadorTotal, ContenedorSalir, e);
				}
			}

			public void keyReleased(KeyEvent e) {
				btnA.setBackground(null);
				btnB.setBackground(null);
				btnC.setBackground(null);
				btnD.setBackground(null);
				btnE.setBackground(null);
				btnF.setBackground(null);
				btnG.setBackground(null);
				btnH.setBackground(null);
				btnI.setBackground(null);
				btnJ.setBackground(null);
				btnK.setBackground(null);
				btnL.setBackground(null);
				btnM.setBackground(null);
				btnN.setBackground(null);
				btn�.setBackground(null);
				btnO.setBackground(null);
				btnP.setBackground(null);
				btnQ.setBackground(null);
				btnR.setBackground(null);
				btnS.setBackground(null);
				btnT.setBackground(null);
				btnU.setBackground(null);
				btnV.setBackground(null);
				btnW.setBackground(null);
				btnX.setBackground(null);
				btnY.setBackground(null);
				btnZ.setBackground(null);
				btn1.setBackground(null);
				btn2.setBackground(null);
				btn3.setBackground(null);
				btn4.setBackground(null);
				btn5.setBackground(null);
				btn6.setBackground(null);
				btn7.setBackground(null);
				btn8.setBackground(null);
				btn9.setBackground(null);
				btn0.setBackground(null);
				btnEspacio.setBackground(null);
			}
		});

		// ContenedorSalir.
		ContenedorSalir.setVisible(false);
		frame.setFocusable(true);

	}

	public void pintaDondeQuieras(JTextPane txtTexto, JLabel l, JLabel lblVariableContadorTotal, JPanel ContenedorSalir, KeyEvent e) {

		Highlighter highlighter = txtTexto.getHighlighter();
		HighlightPainter painter = new DefaultHighlighter.DefaultHighlightPainter(Color.green);
		HighlightPainter painter2 = new DefaultHighlighter.DefaultHighlightPainter(Color.pink);

		int p1 = posicionCursor + 1;
		try { 
		
			if (posicionCursor <= letra.length-1) {
				contadorPulsacionesXMin++;
				if (letra[posicionCursor] == e.getKeyChar()||letra[posicionCursor] == '.' || letra[posicionCursor] == ','
						|| letra[posicionCursor] == ';') {
					highlighter.addHighlight(posicionCursor, p1, painter);
					if(posicionCursor <= letra.length-1) {
					posicionCursor++;
					}
				} else {
					errores++;
					highlighter.addHighlight(posicionCursor, p1, painter2);
					if(posicionCursor <= letra.length-1) {
						posicionCursor++;
						} 
					l.setText(errores + "");
				}
				if(posicionCursor == letra.length) {
					time.stop();
					ContenedorSalir.setVisible(true);
				}
			}
			lblVariableContadorTotal.setText((int) contadorPulsacionesXMin + "");
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}

	}

}
