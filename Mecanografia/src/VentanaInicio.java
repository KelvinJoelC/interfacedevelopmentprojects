import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DecimalFormat;

import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import java.awt.Window.Type;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Point;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.JPasswordField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.DropMode;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Color;
import javax.swing.JCheckBox;
import java.awt.Cursor;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import java.awt.Scrollbar;
import java.awt.SystemColor;

public class VentanaInicio {

	 private static JFrame frmMecanografa;//Principal	
	 private Timer time;
	 static CargarFichero leeFichero = new CargarFichero();
	 private String TextoElegido="";
	 
	 public JFrame getFrame() {
			return frmMecanografa;
		}
	 public void setText(String TextoElegido) {
		 this.TextoElegido=TextoElegido;
	 }
	 public String getText() {
		 return this.TextoElegido;
	 }
	 
	 
	//this
	 
	 private JTextField textField;
	 private JPasswordField passwordField;
	 private static JPasswordField TFPassLogin;
	 private static JFormattedTextField TFUserLogin;
	 private JPanel pnlPantallaCarga;
	 private JPanel pnlPantallaElige;
	 private JPanel pnlPantallaLogin;
	 
	 public void setTFUserLogin(String usuarioActual) {
			
			
		}
	 
	 public String getTFUserLogin() {
		 return TFUserLogin.getText();
	 }

	public static void main(String[] args) {
		

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					VentanaInicio window = new VentanaInicio();
					window.frmMecanografa.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VentanaInicio(String usuarioActual) {
		initialize();
		cargadoLogin(usuarioActual);

	}
	public VentanaInicio() {
		initialize();
		
		//time.start();

	}
	
	private void initialize() {

	//region VARIABLES	
		ImageIcon icon = new ImageIcon("src/Images/DosaIcon.png");
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		int height = pantalla.height;
	    int width = pantalla.width;
	    
	 //Frame Main
	    
		frmMecanografa = new JFrame();
		frmMecanografa.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (JOptionPane.showConfirmDialog(frmMecanografa,
						"Vas a salir del programa\n" + "�Estas seguro?", "Cerrar el programa",
						JOptionPane.YES_NO_OPTION) == 0) {
					frmMecanografa.dispose();
				}else frmMecanografa.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			}
		});
		
		frmMecanografa.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		frmMecanografa.setResizable(false);
		frmMecanografa.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		frmMecanografa.setAlwaysOnTop(true);
		frmMecanografa.setTitle("Mecanograf\u00EDa.");
		frmMecanografa.setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaInicio.class.getResource("/Images/DosaIconMin.png")));


		frmMecanografa.setBounds((height/2),(width/2), 800, 600);
		frmMecanografa.setLocationRelativeTo(null);//Para que te salga centrado
		frmMecanografa.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMecanografa.setVisible(true);
		frmMecanografa.getContentPane().setLayout(null);
		
		
		
		JMenuBar menuBar = new JMenuBar();
		frmMecanografa.setJMenuBar(menuBar);
		
		JMenu mnInformacin = new JMenu("Acerca de");
		menuBar.add(mnInformacin);
		
		JLabel lblVersin = new JLabel("Versi\u00F3n: 1.1");
		mnInformacin.add(lblVersin);
		
		JLabel lblRealizadoPorKelvin = new JLabel("Realizado por: Kelvin Carre\u00F1o");
		mnInformacin.add(lblRealizadoPorKelvin);
		
		JLabel lblUltimaActualizaccin = new JLabel("Ultima actualizaci\u00F3n: 14/11/2019 ");
		mnInformacin.add(lblUltimaActualizaccin);
		
		 pnlPantallaCarga = new JPanel();
		 pnlPantallaCarga.setBackground(SystemColor.inactiveCaption);
		pnlPantallaCarga.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		
		//region PanelPantallaCarga
		pnlPantallaCarga.setBounds(0, 0, 794, 550);
		frmMecanografa.getContentPane().add(pnlPantallaCarga);
		pnlPantallaCarga.setLayout(null);
		
		JProgressBar barraDeProgreso = new JProgressBar();
		
		barraDeProgreso.setBounds(0, 530, 794, 20);
		pnlPantallaCarga.add(barraDeProgreso);
		
		
		
		JLabel lblIcon = new JLabel("");
		lblIcon.setBounds(196, 126, 368, 266);
		pnlPantallaCarga.add(lblIcon);
		lblIcon.setIcon(icon);
		lblIcon.setHorizontalAlignment(SwingConstants.CENTER);
		lblIcon.setVerticalAlignment(SwingConstants.CENTER);
		
		JPanel panelBarraProgreso = new JPanel();
		panelBarraProgreso.setBackground(SystemColor.inactiveCaption);
		panelBarraProgreso.setBounds(302, 426, 159, 36);
		pnlPantallaCarga.add(panelBarraProgreso);
		JLabel lblTextoCarga = new JLabel("");
		panelBarraProgreso.add(lblTextoCarga);
		

	if(compruebaFicheros()==true){
			time = new Timer(10, new ActionListener() {
				int porcentaje=0;
				public void actionPerformed(ActionEvent ae) {
					barraDeProgreso.setValue(porcentaje);
					if(barraDeProgreso.getValue()==20)
					lblTextoCarga.setText("Icono Cargado");	
					if(barraDeProgreso.getValue()==40)
					lblTextoCarga.setText("Cargando textos");
					if(barraDeProgreso.getValue()==60)
						lblTextoCarga.setText("Cargando usuarios");	
					if(barraDeProgreso.getValue()==80)
						lblTextoCarga.setText("Cargando estad�sticas");	
					if(barraDeProgreso.getValue()==100)
						lblTextoCarga.setText("Cargado correctamente");	
					if(barraDeProgreso.getValue()!=100)
					porcentaje++;
					}
			});
			
			time.start();
			
			pnlPantallaCarga.addMouseListener(new MouseAdapter() 
			{
				@Override
				public void mouseClicked(MouseEvent e) 
				{
					if( barraDeProgreso.getValue()==100) 
					{
					//pnlPantallaCarga.setVisible(false); 
					cargaPantallaLogin();
					}
				}
			});			
		
		}else {
			System.exit(0);
			}
	}
	
	public static boolean compruebaFicheros()  {
		
		//leeFichero.cargaFicheroUsuarios();
		if(leeFichero.comprobarFicheros()== false) {
			
			JOptionPane.showMessageDialog(frmMecanografa,"No se encuentra un archivo\n"
					+ "Pulsar para cerrar la aplicaci�n","Mensaje de Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}else return true;			
	}
	

		public void cargaPantallaLogin() {
			
				
				pnlPantallaCarga.setVisible(false); 
				pnlPantallaLogin = new JPanel();
				pnlPantallaLogin.setLayout(null); //Para poner Layout en absoluto
				
				
				pnlPantallaLogin.setBackground(SystemColor.inactiveCaption);
				pnlPantallaLogin.setBounds(0, 0, 794, 550);
				frmMecanografa.getContentPane().add(pnlPantallaLogin);
				
				passwordField = new JPasswordField();
				pnlPantallaLogin.add(passwordField);
				
				textField = new JTextField();
				pnlPantallaLogin.add(textField);
				textField.setColumns(1);
				
				TFUserLogin = new JFormattedTextField();
				TFUserLogin.setFont(new Font("Arial", Font.BOLD, 18));
				TFUserLogin.setHorizontalAlignment(SwingConstants.CENTER);
				TFUserLogin.setBounds(267, 117, 223, 38);
				pnlPantallaLogin.add(TFUserLogin);
				
				JLabel lblUsuario = new JLabel("Insertar usuario");
				lblUsuario.setEnabled(false);
				lblUsuario.setForeground(Color.GRAY);
				lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 11));
				lblUsuario.setHorizontalAlignment(SwingConstants.CENTER);
				lblUsuario.setBounds(263, 157, 103, 14);
				pnlPantallaLogin.add(lblUsuario);
				
				TFPassLogin = new JPasswordField();
				TFPassLogin.setHorizontalAlignment(SwingConstants.CENTER);
				TFPassLogin.setEchoChar('*');
				TFPassLogin.setFont(new Font("Arial", Font.BOLD, 18));
				TFPassLogin.setBounds(267, 200, 223, 38);
				pnlPantallaLogin.add(TFPassLogin);
				
				JLabel lblPassword = new JLabel("Insertar contrase�a");
				lblPassword.setEnabled(false);
				lblPassword.setForeground(Color.GRAY);
				lblPassword.setFont(new Font("Tahoma", Font.BOLD, 11));
				lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
				lblPassword.setBounds(267, 240, 117, 14);
				pnlPantallaLogin.add(lblPassword);
				
				ImageIcon icon = new ImageIcon("src/Images/login.png");
				JLabel lblIcon = new JLabel("");
				lblIcon.setBounds(270, 320, 200, 200);
				pnlPantallaLogin.add(lblIcon);
				lblIcon.setIcon(icon);
				lblIcon.setHorizontalAlignment(SwingConstants.CENTER);
				lblIcon.setVerticalAlignment(SwingConstants.CENTER);
	
				JButton btnLogin = new JButton("Iniciar Sesi\u00F3n");
				btnLogin.setFont(new Font("Century Gothic", Font.BOLD, 15));
				btnLogin.setBounds(307, 264, 142, 38);
				pnlPantallaLogin.add(btnLogin);
		
				
				btnLogin.addMouseListener(new MouseAdapter() 
				{
					@Override
					public void mouseClicked(MouseEvent e) 
						{	
							if(leeFichero.cargaFicheroUsuarios(TFUserLogin.getText(), TFPassLogin.getText())==true) {
								pnlPantallaLogin.setVisible(false);
								cargadoLogin(TFUserLogin.getText());
									

							}else
								JOptionPane.showMessageDialog(frmMecanografa,"Usuario o Contrase�a son incorrectos", "Login Incorrecto", JOptionPane.INFORMATION_MESSAGE);		
						}										
				});		
				
		}
		
		public void cargadoLogin(String textTFUser) {
			pnlPantallaElige = new JPanel();
			pnlPantallaCarga.setVisible(false);
			//pnlPantallaLogin.setVisible(false); 
			pnlPantallaElige.setBackground(SystemColor.inactiveCaption);
			pnlPantallaElige.setVisible(true);
			pnlPantallaElige.setLayout(null); //Para poner Layout en absoluto
			pnlPantallaElige.setBounds(0, 0, 794, 550);
			frmMecanografa.getContentPane().add(pnlPantallaElige);
			

			JButton btnPracticar = new JButton("Practicar");
			btnPracticar.setFont(new Font("Arial", Font.BOLD, 18));
			btnPracticar.setHorizontalAlignment(SwingConstants.CENTER);
			btnPracticar.setBounds(260, 122, 223, 38);
			pnlPantallaElige.add(btnPracticar);
			
			JComboBox comboBox = new JComboBox();
			comboBox.setFont(new Font("Tahoma", Font.BOLD, 14));
			comboBox.setToolTipText("Selecciona archivo");
			comboBox.setEditable(false);
			comboBox.setBounds(485, 122,100, 38);
			pnlPantallaElige.add(comboBox);
			comboBox.addItem("Leccion 1");
			comboBox.addItem("Leccion 2");
			
			
			JButton btnEstadisticas = new JButton("Estadistica");
			btnEstadisticas.setFont(new Font("Arial", Font.BOLD, 18));
			btnEstadisticas.setHorizontalAlignment(SwingConstants.CENTER);
			btnEstadisticas.setBounds(260, 230, 223, 38);
			pnlPantallaElige.add(btnEstadisticas);

			JButton btnCerrarSesion = new JButton("Cerrar Sesi�n");
			btnCerrarSesion.setFont(new Font("Arial", Font.BOLD, 14));
			btnCerrarSesion.setHorizontalAlignment(SwingConstants.CENTER);
			btnCerrarSesion.setBounds(307, 280, 142, 38);
			pnlPantallaElige.add(btnCerrarSesion);
			
			ImageIcon icon = new ImageIcon("src/Images/teclado.png");
			JLabel lblIcon = new JLabel("");
			lblIcon.setBounds(200, 320, 356, 200);
			pnlPantallaElige.add(lblIcon);
			lblIcon.setIcon(icon);
			lblIcon.setHorizontalAlignment(SwingConstants.CENTER);
			lblIcon.setVerticalAlignment(SwingConstants.CENTER);
			
			btnCerrarSesion.addMouseListener(new MouseAdapter() 
			{
				@Override
				public void mouseClicked(MouseEvent e) 
					{		
					 
					cargaPantallaLogin();
					pnlPantallaElige.setVisible(false);
					TFUserLogin.setText("");
					TFPassLogin.setText("");							
					}										
			});		
			
			btnPracticar.addMouseListener(new MouseAdapter() 
			{
				@Override
				public void mouseClicked(MouseEvent e) 
					{		
					 
					
							if(comboBox.getSelectedItem().toString() == "Leccion 1") {
								VentanaPrincipal ventanaPrincipal= new VentanaPrincipal(0,textTFUser,1+"" );
								ventanaPrincipal.getFrame().setVisible(true);
								frmMecanografa.dispose();
							}
								
							
							else if(comboBox.getSelectedItem().toString() == "Leccion 2") {
								VentanaPrincipal ventanaPrincipal= new VentanaPrincipal(1,textTFUser,2+"");
								ventanaPrincipal.getFrame().setVisible(true);
								frmMecanografa.dispose();
							}
						
							
					}										
			});		
			
			btnEstadisticas.addMouseListener(new MouseAdapter() 
			{
				@Override
				public void mouseClicked(MouseEvent e) 
					{
					pnlPantallaElige.setVisible(false);
					JPanel pnlPantallaEstadisticas = new JPanel();
					pnlPantallaEstadisticas.setLayout(null); //Para poner Layout en absoluto
					
					pnlPantallaEstadisticas.setBackground(SystemColor.inactiveCaption);
					pnlPantallaEstadisticas.setBounds(0, 0, 794, 550);
					frmMecanografa.getContentPane().add(pnlPantallaEstadisticas);
					
					JScrollPane scrollPane = new JScrollPane();
					scrollPane.setBounds(300, 11, 210, 494);
					pnlPantallaEstadisticas.add(scrollPane);
					
					JTextArea txtEstadisticas = new JTextArea();
					txtEstadisticas.setFont(new Font("Arial", Font.BOLD, 16));
					txtEstadisticas.setEditable(false);
					txtEstadisticas.setAutoscrolls(true);
					txtEstadisticas.setText(leeFichero.muestraEstadisticas(textTFUser));
					txtEstadisticas.setBounds(300, 11, 210, 494);
					//txtEstadisticas.setCaretPosition();			//Para cuando comience sea arriba 
					pnlPantallaEstadisticas.add(txtEstadisticas, BorderLayout.CENTER);
					scrollPane.setViewportView(txtEstadisticas);
					
					ImageIcon icon = new ImageIcon("src/Images/registro.png");
					JLabel lblIcon = new JLabel("");
					lblIcon.setBounds(60, 100, 185,300);
					pnlPantallaEstadisticas.add(lblIcon);
					lblIcon.setIcon(icon);
					lblIcon.setHorizontalAlignment(SwingConstants.CENTER);
					lblIcon.setVerticalAlignment(SwingConstants.CENTER);
					
					JButton btnVolverEleccion = new JButton("Volver");
					btnVolverEleccion.setFont(new Font("Arial", Font.BOLD, 18));
					btnVolverEleccion.setHorizontalAlignment(SwingConstants.CENTER);
					btnVolverEleccion.setBounds(570, 400, 100, 38);
					pnlPantallaEstadisticas.add(btnVolverEleccion);
					
					JLabel lblUsuarioActual = new JLabel("");
					lblUsuarioActual.setFont(new Font("Tahoma", Font.BOLD, 20));
					lblUsuarioActual.setHorizontalAlignment(SwingConstants.CENTER);
					lblUsuarioActual.setText("Usuario: "+textTFUser);
					lblUsuarioActual.setBounds(30, 30, 200, 38);
					lblUsuarioActual.setBackground(Color.BLACK);
					pnlPantallaEstadisticas.add(lblUsuarioActual);
					pnlPantallaEstadisticas.setVisible(true);
					
					btnVolverEleccion.addMouseListener(new MouseAdapter() 
					{
						@Override
						public void mouseClicked(MouseEvent e) 
							{		
							pnlPantallaEstadisticas.setVisible(false);
							pnlPantallaElige.setVisible(true);
					
							}										
					});	
					}										
			});	
		}
		
		
		public static void limpia() {
			
	
			
		}
		
}
